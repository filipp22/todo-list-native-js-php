<?php

namespace App;

use App\DataBase as DB;


class ToDO
{
    private $request;

    private $minLengthNameProject = 3;

    function __construct($request)
    {
        $this->request = $request;
    }

    public function init()
    {
        switch (@$this->request['type'] ? $this->request['type'] : "") {
            case 'getAllProjects':
                $result = $this->getAllProjects();
                break;

            case 'createProject':
                $result = $this->createProject();
                break;

            case 'deleteProject':
                $result = $this->deleteProject();
                break;

            case 'editProject':
                $result = $this->editProject();
                break;

            case 'updateProjectSort':
                $result = $this->updateProjectSort();
                break;

            default:
                $result = [];
        }

        $this->dataInJson($result);


    }

    private function updateProjectSort(){
        $errors = [];

        if (!count(json_decode(@$this->request['sort'])))
            $errors[] = "Нет проектов";


        if (!count($errors)) {
            try {
                DB::beginTransaction();
                foreach (json_decode(@$this->request['sort']) as $item) {
                    $sort = DB::prepare("UPDATE `projects` SET `sort` = :sort WHERE `projects`.`id` = :id");
                    $sort->execute([
                        ":id" => (int)$item->id,
                        ":sort" => (int)$item->sort
                    ]);
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                $errors[] = $e->getMessage();
            }

        }


        return [
            "result" => !count($errors) ? true : false,
            "errors" => $errors
        ];
    }

    private function editProject()
    {
        $errors = [];

        if (!$projectId = intval(@$this->request['id']))
            $errors[] = "Нет ID";


        if (!count(json_decode(@$this->request['tasks'])))
            $errors[] = "Нет задач";


        if (!count($errors)) {
            try {
                DB::beginTransaction();
                $task = DB::prepare("DELETE FROM `tasks` WHERE `project_id` = :project_id");
                $task->execute(['project_id' => $projectId]);
                foreach (json_decode($this->request['tasks']) as $task) {
                    $taskInsert = DB::prepare("INSERT INTO `tasks` (`project_id`, `task`, `checked`, `sort`, `created_at`) VALUES (:project_id, :task, :checked, :sort, now())");

                    $taskInsert->execute([
                        ":project_id" => $projectId,
                        ":task" => $task->task,
                        ":checked" => (int)$task->checked,
                        ":sort" => (int)$task->sort
                    ]);
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                $errors[] = $e->getMessage();
            }

        }


        return [
            "result" => !count($errors) ? true : false,
            "errors" => $errors
        ];
    }


    private function deleteProject()
    {

        $errors = [];

        if (!$id = intval(@$this->request['id']))
            $errors[] = "Нет ID";


        if (!count($errors)) {
            try {
                DB::beginTransaction();
                $project = DB::prepare("DELETE FROM `projects` WHERE `id` = :id");
                $project->execute(['id' => $id]);

                $task = DB::prepare("DELETE FROM `tasks` WHERE `project_id` = :project_id");
                $task->execute(['project_id' => $id]);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                $errors[] = $e->getMessage();
            }

        }


        return [
            "result" => !count($errors) ? true : false,
            "errors" => $errors
        ];
    }

    private function createProject()
    {

        $errors = [];


        if (@$this->request['name'] >= $this->minLengthNameProject)
            $errors[] = "Минимальная длина в название проекта {$this->minLengthNameProject} символа";

        if (!count(json_decode(@$this->request['tasks'])))
            $errors[] = "Нет задач";


        if (!count($errors)) {
            try {
                DB::beginTransaction();
                $project = DB::prepare("INSERT INTO `projects` (`id`, `name`, `sort`,`created_at`) VALUES (NULL, ?, ?, now())");
                $project->execute([$this->request['name'], 0]);
                $projectId = DB::lastInsertId();
                foreach (json_decode($this->request['tasks']) as $task) {
                    $taskInsert = DB::prepare("INSERT INTO `tasks` (`project_id`, `task`, `checked`, `sort`, `created_at`) VALUES (:project_id, :task, :checked, :sort, now())");

                    $taskInsert->execute([
                        ":project_id" => $projectId,
                        ":task" => $task->task,
                        ":checked" => (int)$task->checked,
                        ":sort" => (int)$task->sort
                    ]);
                }
                DB::commit();

            } catch (\Exception $e) {
                DB::rollBack();
                $errors[] = $e->getMessage();
            }

        }


        return [
            "result" => !count($errors) ? true : false,
            "errors" => $errors
        ];

    }

    private function getAllProjects()
    {
        $result = [];

        $stmt = DB::run("SELECT * FROM `projects` ORDER BY `projects`.`sort` ASC")->fetchAll();

        foreach ($stmt as $item) {
            $taskList = DB::run("SELECT * FROM `tasks` WHERE `project_id` = '{$item['id']}'");
            $result[] = [
                "id" => $item['id'],
                "name" => $item['name'],
                "task_count" => $taskList->rowCount(),
                "created_at" => $item['created_at'],
                "taskList" => $taskList->fetchAll()
            ];
        }

        return $result;
    }

    private function dataInJson($data)
    {


        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }

}