<?php

namespace App;

use \PDO;

class DataBase
{
    protected static $instance = null;

    public static function instance()
    {
        if (self::$instance === null) {
            $iniFile = "config.ini";

            try {
                if (($ini = @parse_ini_file($iniFile, true)) == false)
                    throw new Exception('Missing INI file: ' . $iniFile);
                $opt = array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES => TRUE,
                );
                $dsn = 'mysql:host=' . $ini['DB_HOST'] . ';dbname=' . $ini['DB_DATABASE'] . ';charset=utf8';
                self::$instance = new PDO($dsn, $ini['DB_USERNAME'], $ini['DB_PASSWORD'], $opt);
            } catch (Exception $e) {
                die($e->getMessage());
            }

        }
        return self::$instance;
    }

    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::instance(), $method), $args);
    }

    public static function run($sql, $args = [])
    {
        if (!$args) {
            return self::instance()->query($sql);
        }
        $stmt = self::instance()->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }
}
