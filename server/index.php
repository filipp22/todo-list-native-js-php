<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With, content-type');
spl_autoload_register(function ($classname) {
    require_once("./" . $classname . ".php");
});

$app = new App\ToDO($_POST);

$app->init();
