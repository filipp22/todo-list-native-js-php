class ToDo {

    state = {
        isLoading: true,
        minLengthTask: 3,
        minLengthNameProject: 3,
        taskList: [],
        projectList: [
            {
                id: 0,
                name: 'TODO',
                task_count: 2,
                created_at: '16.10.2020, 16:51:36',
                taskList: [
                    {task: "Доделать клиент", checked: true, id: 1, sort: 1},
                    {task: "Начать делать server side", checked: false, id: 2, sort: 2},
                ]
            },
            {
                id: 1,
                name: 'Первый проект',
                task_count: 2,
                created_at: '16.10.2020, 16:51:36',
                taskList: [
                    {task: "Первая Задача", checked: false, id: 1, sort: 1},
                    {task: "Вторая Задача", checked: false, id: 2, sort: 2},
                ]
            },
            {
                id: 2,
                name: 'Второй проект',
                task_count: 2,
                created_at: '16.10.2020, 16:51:36',
                taskList: [
                    {task: "Первая Задача", checked: false, id: 1, sort: 1},
                    {task: "Вторая Задача", checked: false, id: 2, sort: 2},
                ]
            },
            {
                id: 3,
                name: 'Третий проект',
                task_count: 4,
                created_at: '16.10.2020, 16:51:36',
                taskList: [
                    {task: "Первая Задача Первая ЗадачаПервая ЗадачаПервая Задача", checked: false, id: 1, sort: 1},
                    {task: "Вторая Задача Вторая Задача Вторая Задача Вторая Задача", checked: false, id: 2, sort: 2},
                ]
            },
            {
                id: 4,
                name: 'Мега проект',
                task_count: 4,
                created_at: '16.10.2020, 16:51:36',
                taskList: [
                    {task: "Первая Задача Первая ЗадачаПервая ЗадачаПервая Задача", checked: false, id: 1, sort: 1},
                    {task: "Вторая Задача Вторая Задача Вторая Задача Вторая Задача", checked: false, id: 2, sort: 2},
                ]
            }

        ]
    };


    constructor() {

    }

    findProjectById = (id) => this.state.projectList.filter(e => e.id == id)


    editProject = () => {

        const name = document.getElementsByClassName('project-name')[0]

        const id = document.getElementById('project-edit-name').getAttribute('data-id')

        const taskList = document.querySelectorAll('.edit-list li')

        if (taskList.length == 0) {
            alert("Добавте задачу!")
        } else {
            let tasks = []
            for (let i = 0; i < taskList.length; i++) {
                tasks.push({
                    task: taskList[i].querySelector('.container span').textContent,
                    checked: taskList[i].querySelector('.check-with-label').checked,
                    sort: i
                })
            }

            const data = {
                type: 'editProject',
                id: id,
                tasks: JSON.stringify(tasks)
            }


            this.callAPI(server, data, (response) => {
                document.body.style.overflow = 'visible';
                document.querySelector('#projectModal').style.marginLeft = '0px';
                if (response.result) {
                    toast['success']({
                        message: 'Проект изменен',
                        position: 'south-east',
                        timeout: 5000
                    })

                    this.updateState()
                } else {
                    toast['error']({
                        message: 'Проект не добавлен',
                        position: 'south-east',
                        timeout: 5000
                    })
                }
                this.closeEditProject()
            })
        }

    }

    createProject = () => {

        const name = document.getElementsByClassName('project-name')[0]

        const taskList = document.querySelectorAll('.create-list li')

        if (name.value.length <= this.state.minLengthNameProject) {
            alert("Минимальная длина в название проекта " + this.state.minLengthNameProject + " символа")
        } else {
            if (taskList.length == 0) {
                alert("Добавте задачу!")
            } else {
                let tasks = []
                for (let i = 0; i < taskList.length; i++) {
                    tasks.push({
                        task: taskList[i].querySelector('.container span').textContent,
                        checked: taskList[i].querySelector('.check-with-label').checked,
                        sort: i
                    })
                }

                const data = {
                    type: 'createProject',
                    name: name.value,
                    tasks: JSON.stringify(tasks)
                }

                this.callAPI(server, data, (response) => {
                    let link = document.createElement('a');
                    link.href = "#close";
                    link.click();
                    document.body.style.overflow = 'visible';
                    document.querySelector('#createProject').style.marginLeft = '0px';
                    if (response.result) {
                        toast['success']({
                            message: 'Проект добавлен',
                            position: 'south-east',
                            timeout: 5000
                        })

                        this.updateState()
                    } else {
                        toast['error']({
                            message: 'Проект не добавлен',
                            position: 'south-east',
                            timeout: 5000
                        })
                    }
                })
            }
        }
    }

    deleteProject = (id) => {
        if (confirm('Удалить ' + this.findProjectById(id)[0].name + ' ?')) {


            const data = {
                type: 'deleteProject',
                id: id
            }

            this.callAPI(server, data, (response) => {
                if (response.result) {
                    this.state.projectList.splice(this.state.projectList.findIndex(e => e.id === id), 1)
                    toast['success']({
                        message: 'Проект удален',
                        position: 'south-east',
                        timeout: 5000
                    })
                    this.updateState()
                } else {
                    toast['error']({
                        message: 'Проект не удален',
                        position: 'south-east',
                        timeout: 5000
                    })
                }
            })


        }
    }

    deleteTaskInCreateProject = (el) => {
        el.parentElement.parentElement.remove()
    }

    hideLoading = () => document.getElementsByTagName('body')[0].classList.remove("loading")

    showLoading = () => document.getElementsByTagName('body')[0].classList.add("loading")

    createProjectAddTaskList = (type = 0) => {
        const task = document.getElementsByClassName("task-add")[type];
        if (task.value.length <= this.state.minLengthTask) {
            alert("Задача слишком короткая")
        } else {
            let data = {task: task.value, id: 1, sort: 1, checked: false}
            this.state.taskList.push();
            document.getElementsByClassName('task_list')[type].insertAdjacentHTML('beforeend', this.appendTaskList(data))
            new DragonDrop('.task_list .task_list_item')
            task.value = "";
        }
        return this
    }

    appendTaskList = (task) => {
        return this.templateTask(task);
    }

    openEditProject = (id) => {

        const nameProject = document.getElementById('project-edit-name')
        nameProject.innerHTML = ""
        nameProject.removeAttribute('data-id')
        document.getElementsByClassName('task_list')[1].innerHTML = ""
        document.querySelector('#projectModal').classList.add('open-modal')
        let data = this.findProjectById(id)[0]
        nameProject.innerHTML = data.name
        nameProject.setAttribute('data-id', id)
        data.taskList.map((v, i) => {
            document.getElementsByClassName('task_list')[1].insertAdjacentHTML('beforeend', this.appendTaskList(v))
        })
        new DragonDrop('.task_list .task_list_item')


    }

    closeEditProject = () => {
        document.querySelector('#projectModal').classList.remove('open-modal')
        this.hideAllDropMenu()
    }

    renderProjectList = () => {
        let result = "";
        let items = document.querySelectorAll('.project__list_item');
        for (var i = 0; i < items.length; i++) {
            items[i].remove();
        }
        if (this.state.projectList.length) {
            this.state.projectList.map((item, i) => {
                result += this.templateProject(item)
            });
        }
        document.getElementsByClassName('project__list')[0].insertAdjacentHTML('afterBegin', result)
    }


    templateTask = (data) => {
        let checked = data.checked == 1 ? 'checked' : ''
        return `<li class="task_list_item" draggable="true">
                            <div class="item">
                                <label class="container">
                                    <input type="checkbox" name="isTask" class="check-with-label" ${checked}/>
                                    <span>${data.task}</span>
                                </label>
                                <div onclick="app.deleteTaskInCreateProject(this)" data-id="${data.id}" class="close">×</div>
                            </div>
                            <div class="move_block"><span>&#10267;</span></div>
                        </li>`
    }

    templateProject = (data) => {
        let name = data.name
        let taskList = "";
        data.taskList.map((v, i) => {
            if (i < 2)
                return taskList += `<li>${v.task}</li>`
        })
        return `<li data-id="${data.id}" class="project__list_item" draggable="true">
            <div class="card">
                <div class="title">${name}</div>
                <ul class="task__list">${taskList}</ul>
                <div class="count_task"><span>${data.task_count}</span></div>
                <div class="dropdown">
                    <a href="#" class="js-link">&#10247;</a>
                    <ul class="js-dropdown-list">
                        <li onclick="app.openEditProject(${data.id})">Редактировать</li>
                        <li onclick="app.deleteProject(${data.id})">Удалить</li>
                    </ul>
                </div>
                <div class="move_block"><span>&#10267;</span></div>
            </div>
        </li>
        `
    }

    updateState = () => {
        this.init()
    }

    callAPI(url = '', data = {}, callback) {

        var str = Object.keys(data).map(function (key) {
            return key + '=' + data[key];
        }).join('&');

        fetch(url, {
            method: "POST",
            body: str,
            headers: {"content-type": "application/x-www-form-urlencoded"}
        })
            .then((response) => {
                if (response.status !== 200) {
                    return Promise.reject();
                }
                return response.text()
            })
            .then(response => {
                callback(JSON.parse(response))
            })
            .catch(() => console.log('ошибка'));
    }

    updateProjectSort = () => {
        const items = document.querySelectorAll('.project__list_item')
        let sort = []
        for (let i = 0; i < items.length; i++) {
            sort.push({
                id: items[i].getAttribute('data-id'),
                sort: i
            })
        }
        const data = {
            type: 'updateProjectSort',
            sort: JSON.stringify(sort)
        }

        this.callAPI(server, data, (response) => {
            if (response.result) {
                toast['success']({
                    message: 'Сортировка сохранена',
                    position: 'south-east',
                    timeout: 5000
                })
            } else {
                toast['error']({
                    message: 'Сортировка не сохранена',
                    position: 'south-east',
                    timeout: 5000
                })
            }
        })
        console.log(sort)
    }

    init = () => {

        const data = {
            type: 'getAllProjects'
        }

        this.callAPI(server, data, (response) => {
            this.state.projectList = response
            this.renderProjectList()
            new DragonDrop('.project__list .project__list_item')
            new DragonDrop('.task_list .task_list_item')
            this.dropdownInCard()
            this.hideLoading()
        })


        return this
    }

    hideAllDropMenu = () => {
        const dropdownBtns = document.getElementsByClassName("dropdown");
        for (let i = 0; i < dropdownBtns.length; i++) {
            dropdownBtns[i].querySelector('ul').classList.remove("current")
        }
    }

    dropdownInCard = () => {
        const dropdownBtns = document.getElementsByClassName("dropdown");
        const self = this
        for (let i = 0; i < dropdownBtns.length; i++) {
            dropdownBtns[i].addEventListener("click", function () {
                self.hideAllDropMenu()
                this.querySelector('ul').classList.toggle("current");
            }, false);
        }
        return this
    }


}

const app = new ToDo().init()


let dragSrcEl = null;

function handleDragStart(e) {

    dragSrcEl = this;
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', this.outerHTML);
    this.classList.add('dragElem');
}

function handleDragOver(e) {
    if (e.preventDefault) {
        e.preventDefault();
    }
    this.classList.add('over');
    e.dataTransfer.dropEffect = 'move';
    return false;
}

function handleDragEnter(e) {

}

function handleDragLeave(e) {

    this.classList.remove('over');
}

function handleDrop(e) {
    if (e.stopPropagation) {
        e.stopPropagation();
    }
    if (dragSrcEl != this) {
        this.parentNode.removeChild(dragSrcEl);
        let dropHTML = e.dataTransfer.getData('text/html');
        this.insertAdjacentHTML('beforebegin', dropHTML);
        let dropElem = this.previousSibling;
        addDnDHandlers(dropElem);

    }
    this.classList.remove('over');
    return false;
}

function handleDragEnd(e) {
    this.classList.remove('over');
    app.dropdownInCard()
    app.updateProjectSort()
}

function addDnDHandlers(elem) {
    elem.addEventListener('dragstart', handleDragStart, false);
    elem.addEventListener('dragenter', handleDragEnter, false)
    elem.addEventListener('dragover', handleDragOver, false);
    elem.addEventListener('dragleave', handleDragLeave, false);
    elem.addEventListener('drop', handleDrop, false);
    elem.addEventListener('dragend', handleDragEnd, false);

}

class DragonDrop {

    dragSrcEl = null;

    constructor(selector) {
        let cols = document.querySelectorAll(selector);
        [].forEach.call(cols, addDnDHandlers);

    }
}




