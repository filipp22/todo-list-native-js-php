document.addEventListener("DOMContentLoaded", function () {
    document.querySelector('[href="#createProject"]').addEventListener('click', function () {
        const name = document.getElementsByClassName('project-name')[0]
        name.value = ""
        document.querySelectorAll('.task_list li').forEach(e => e.parentNode.removeChild(e));
        document.body.style.overflow = 'hidden';
        document.querySelector('#createProject').style.marginLeft = document.body.clientWidth - window.innerWidth + 'px';
    });
    document.querySelector('[href="#"]').addEventListener('click', function () {
        document.body.style.overflow = 'visible';
        document.querySelector('#createProject').style.marginLeft = '0px';
    });
});
